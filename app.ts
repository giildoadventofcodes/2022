import dayFunction from './days/day12.2'

let start = (new Date()).getTime()
let response = dayFunction()
console.log(response)
let end = (new Date()).getTime()
console.log(`${end - start}ms`)
