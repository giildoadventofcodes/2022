import fs from 'node:fs'
import path from 'node:path'

export default (): number => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day1.txt'), { encoding: 'utf8' })
           .split('\n')
           .reduce<number[]>((elfs, row, i, calories) => {
             if (row === '') {
               if (i === calories.length - 1) return elfs
               elfs.push(0)
               return elfs
             }

             elfs[elfs.length - 1] += parseInt(row, 10)
             return elfs
           }, [0])
           .sort((a, b) => b - a)
           .shift() as number
}

