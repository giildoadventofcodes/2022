import { day10_test } from '../data/day10'

interface Signal {
  type: 'addx' | 'noop'
  value?: number
}

class SignalReader {
  signals: Signal[]

  screen: ('█' | ' ')[][]

  constructor() {
    this.signals = day10_test.split('\n')
                             .map((signal) => {
                               const [type, value] = signal.split(' ')
                               if (type === 'addx') {
                                 return { type: 'addx', value: parseInt(value) }
                               }

                               return { type: 'noop' }
                             })

    this.screen = []

    this.reader()
  }

  private drawPixel(i: number, spritePosition: number): void {
    const line = Math.ceil((i / 40)) - 1
    const column = (i- 1) % 40
    if (column === 0) {
      this.screen[line] = []
    }

    this.screen[line][column] = spritePosition === column + 1 ||
    spritePosition + 1 === column + 1 ||
    spritePosition + 2 === column + 1
      ? '█' : ' '
  }

  private reader(): void {
    let addxLoopI = 0
    let i = 0
    let signal: Signal | undefined
    let spritePosition = 1

    while (this.signals.length) {
      i++
      if (addxLoopI === 1) {
        this.drawPixel(i, spritePosition)
        addxLoopI = 0
        spritePosition += signal!.value!
        continue
      }

      signal = this.signals.shift()
      if (signal === undefined) break

      if (signal.type === 'noop') {
        this.drawPixel(i, spritePosition)
        continue
      }

      if (signal.type === 'addx') {
        this.drawPixel(i, spritePosition)
        addxLoopI++
        continue
      }
    }
  }

  public getX(): string {
    return this.screen.reduce((acc, line) => {
      return acc + line.join('') + '\n'
    }, '')
  }
}

export default (): string => {
  return new SignalReader().getX()
}
