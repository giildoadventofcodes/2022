import { day10_test } from '../data/day10'

interface Signal {
  type: 'addx' | 'noop'
  value?: number
}

class SignalReader {
  signals: Signal[]

  XSum: number

  constructor() {
    this.signals = day10_test.split('\n')
                             .map((signal) => {
                               const [type, value] = signal.split(' ')
                               if (type === 'addx') {
                                 return { type: 'addx', value: parseInt(value) }
                               }

                               return { type: 'noop' }
                             })

    this.XSum = 0

    this.reader()
  }

  private reader(): void {
    let addxLoopI = 0
    let i = 0
    let signal: Signal | undefined
    let X = 1

    while (this.signals.length) {
      i++
      if (i === 20 || (i - 20) % 40 === 0) {
        this.XSum += X * i
      }

      if (addxLoopI === 1) {
        addxLoopI = 0
        X += signal!.value!
        continue
      }

      signal = this.signals.shift()
      if (signal === undefined) break

      if (signal.type === 'noop') {
        continue
      }

      if (signal.type === 'addx') {
        addxLoopI++
        continue
      }
    }
  }

  public getX(): number {
    return this.XSum
  }
}

export default (): number => {
  return new SignalReader().getX()
}
