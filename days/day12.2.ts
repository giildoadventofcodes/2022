import { day12 } from '../data/day12'

interface Path {
  x: number;
  y: number;
  elevation: number;
  pathsAlreadyTaken: Set<string>;
}

class MapReader {
  private map: number[][]
  private startCoordinates: { x: number, y: number }
  private paths: Path[]
  private coordinateAlreadyVisited: Map<string, number>

  constructor() {
    this.startCoordinates = { x: 0, y: 0 }
    this.paths = []
    this.coordinateAlreadyVisited = new Map()

    this.map = day12.split('\n').map((row, y) => row.split('').map((char, x) => {
      if (char === 'S') {
        return 'a'.charCodeAt(0) - 1
      }

      if (char === 'E') {
        this.startCoordinates = { x, y }
        return 'z'.charCodeAt(0) + 1
      }

      return char.charCodeAt(0)
    }))
    this.readMap()
  }

  private readMap() {
    let security = 0
    let endFound = false
    this.paths = [{
      x: this.startCoordinates.x,
      y: this.startCoordinates.y,
      elevation: this.map[this.startCoordinates.y][this.startCoordinates.x],
      pathsAlreadyTaken: new Set([JSON.stringify(this.startCoordinates)]),
    }]
    this.coordinateAlreadyVisited.set(JSON.stringify(this.startCoordinates), 0)
    let clonePath = []

    while (!endFound && security < 600) {
      clonePath = [...this.paths]
      this.paths = []

      for (const path of clonePath) {
        const t = this.readDirection('t', { ...path })
        if (t) {
          if (t.elevation === 'a'.charCodeAt(0)) endFound = true
          this.paths.push(t)
        }

        const l = this.readDirection('l', { ...path })
        if (l) {
          if (l.elevation === 'a'.charCodeAt(0)) endFound = true
          this.paths.push(l)
        }

        const r = this.readDirection('r', { ...path })
        if (r) {
          if (r.elevation === 'a'.charCodeAt(0)) endFound = true
          this.paths.push(r)
        }

        const b = this.readDirection('b', { ...path })
        if (b) {
          if (b.elevation === 'a'.charCodeAt(0)) endFound = true
          this.paths.push(b)
        }
      }

      security++
    }
  }

  private readDirection(direction: 't' | 'l' | 'r' | 'b', path: Path): false | Path {
    let currentCoordinates: string
    switch (direction) {
      case 't':
        if (path.y === 0) return false

        currentCoordinates = JSON.stringify({ x: path.x, y: path.y - 1 })
        if (path.pathsAlreadyTaken.has(currentCoordinates)) return false

        if (this.coordinateAlreadyVisited.has(currentCoordinates)) {
          if (this.coordinateAlreadyVisited.get(currentCoordinates)! <= path.pathsAlreadyTaken.size) return false
        }

        if (path.elevation - this.map[path.y - 1][path.x] > 1) return false

        return this.getPath(path.x, path.y - 1, path)
      case 'l':
        if (path.x === 0) return false

        currentCoordinates = JSON.stringify({ x: path.x - 1, y: path.y })
        if (path.pathsAlreadyTaken.has(currentCoordinates)) return false

        if (this.coordinateAlreadyVisited.has(currentCoordinates)) {
          if (this.coordinateAlreadyVisited.get(currentCoordinates)! <= path.pathsAlreadyTaken.size) return false
        }

        if (path.elevation - this.map[path.y][path.x - 1] > 1) return false

        return this.getPath(path.x - 1, path.y, path)
      case 'r':
        if (path.x === this.map[path.y].length - 1) return false

        currentCoordinates = JSON.stringify({ x: path.x + 1, y: path.y })
        if (path.pathsAlreadyTaken.has(currentCoordinates)) return false

        if (this.coordinateAlreadyVisited.has(currentCoordinates)) {
          if (this.coordinateAlreadyVisited.get(currentCoordinates)! <= path.pathsAlreadyTaken.size) return false
        }

        if (path.elevation - this.map[path.y][path.x + 1] > 1) return false

        return this.getPath(path.x + 1, path.y, path)
      case 'b':
        if (path.y === this.map.length - 1) return false

        currentCoordinates = JSON.stringify({ x: path.x, y: path.y + 1 })
        if (path.pathsAlreadyTaken.has(currentCoordinates)) return false

        if (this.coordinateAlreadyVisited.has(currentCoordinates)) {
          if (this.coordinateAlreadyVisited.get(currentCoordinates)! <= path.pathsAlreadyTaken.size) return false
        }

        if (path.elevation - this.map[path.y + 1][path.x] > 1) return false

        return this.getPath(path.x, path.y + 1, path)
    }
  }

  private getPath(x: number, y: number, path: Path): Path {
    const currentCoordinates = JSON.stringify({ x, y })
    const pathsAlreadyTaken = new Set(path.pathsAlreadyTaken)
    pathsAlreadyTaken.add(currentCoordinates)
    this.coordinateAlreadyVisited.set(currentCoordinates, pathsAlreadyTaken.size - 1)

    return {
      x,
      y,
      elevation: this.map[y][x],
      pathsAlreadyTaken,
    }
  }

  public minPath(): number {
    return Math.min(
      ...this.paths.map(path => path.pathsAlreadyTaken.size - 1),
    )
  }
}

export default (): number => {
  return new MapReader().minPath()
}
