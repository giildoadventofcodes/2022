import fs from 'node:fs'
import path from 'node:path'

type FightType = 'A' | 'B' | 'C'
type TypeResult = 'X' | 'Y' | 'Z'
type WinStatus = 'L' | 'D' | 'W'

enum FightTypeEnum {
  A = 'Rock',
  B = 'Paper',
  C = 'Scissors',
}

enum MeFightEnum {
  Rock = 'A',
  Paper = 'B',
  Scissors = 'C',
}

enum ResultFight {
  X = 'L',
  Y = 'D',
  Z = 'W',
}

interface FightValue {
  enemy: FightType;
  typeResult: TypeResult;
}

class RockPaperScissors {
  private fights: FightValue[]

  constructor(rows: string) {
    this.fights = rows.split('\n')
                      .filter((row) => row !== '')
                      .map<FightValue>(row => {
                        const [enemy, typeResult] = row.split(' ') as [FightType, ' ', TypeResult]
                        return {
                          enemy: enemy as FightType,
                          typeResult: typeResult as TypeResult,
                        }
                      })
  }

  private typeValue(type: FightType): number {
    switch (type) {
      case MeFightEnum.Rock:
        return 1
      case MeFightEnum.Paper:
        return 2
      case MeFightEnum.Scissors:
        return 3
      default:
        return 0
    }
  }

  private statusValue(status: WinStatus | undefined): number {
    switch (status) {
      case 'L':
        return 0
      case 'D':
        return 3
      case 'W':
        return 6
      default:
        return 0
    }
  }

  private fightResult(fight: FightValue): FightType | undefined {
    const { enemy, typeResult } = fight

    switch (FightTypeEnum[enemy]) {
      case 'Rock':
        switch (ResultFight[typeResult]) {
          case 'L':
            return MeFightEnum.Scissors
          case 'D':
            return MeFightEnum.Rock
          case 'W':
            return MeFightEnum.Paper
        }
        break
      case 'Paper':
        switch (ResultFight[typeResult]) {
          case 'L':
            return MeFightEnum.Rock
          case 'D':
            return MeFightEnum.Paper
          case 'W':
            return MeFightEnum.Scissors
        }
        break
      case 'Scissors':
        switch (ResultFight[typeResult]) {
          case 'L':
            return MeFightEnum.Paper
          case 'D':
            return MeFightEnum.Scissors
          case 'W':
            return MeFightEnum.Rock
        }
        break
    }
  }

  public result(): number {
    return this.fights.reduce<number>((sum, fight) => {
      const me = this.fightResult(fight)
      return sum + (me ? this.typeValue(me) : 0) + this.statusValue(ResultFight[fight.typeResult])
    }, 0)
  }
}

export default (): number => {
  const fights = new RockPaperScissors(fs.readFileSync(path.join(process.cwd(), '/data/day2.txt'), { encoding: 'utf8' }),
  )
  return fights.result()
}

