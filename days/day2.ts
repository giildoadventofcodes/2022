import fs from 'node:fs'
import path from 'node:path'

type EnemyType = 'A' | 'B' | 'C'
type MeType = 'X' | 'Y' | 'Z'
type WinStatus = 'L' | 'D' | 'W'

enum EnemyFight {
  A = 'Rock',
  B = 'Paper',
  C = 'Scissors',
}

enum MeFight {
  X = 'Rock',
  Y = 'Paper',
  Z = 'Scissors',
}

interface FightValue {
  enemy: EnemyType;
  me: MeType;
}

class RockPaperScissors {
  private fights: FightValue[]

  constructor(rows: string) {
    this.fights = rows.split('\n')
                      .filter((row) => row !== '')
                      .map<FightValue>(row => {
                        const [enemy, me] = row.split(' ') as [EnemyType, ' ', MeType]
                        return {
                          enemy: enemy as EnemyType,
                          me: me as MeType,
                        }
                      })
  }

  private typeValue(type: MeType): number {
    switch (type) {
      case 'X':
        return 1
      case 'Y':
        return 2
      case 'Z':
        return 3
    }
  }

  private statusValue(status: WinStatus | undefined): number {
    switch (status) {
      case 'L':
        return 0
      case 'D':
        return 3
      case 'W':
        return 6
      default:
        return 0
    }
  }

  private fightResult(fight: FightValue): WinStatus | undefined {
    const { enemy, me } = fight

    switch (EnemyFight[enemy]) {
      case 'Rock':
        switch (MeFight[me]) {
          case 'Paper':
            return 'W'
          case 'Rock':
            return 'D'
          case 'Scissors':
            return 'L'
        }
        break
      case 'Paper':
        switch (MeFight[me]) {
          case 'Scissors':
            return 'W'
          case 'Paper':
            return 'D'
          case 'Rock':
            return 'L'
        }
        break
      case 'Scissors':
        switch (MeFight[me]) {
          case 'Rock':
            return 'W'
          case 'Scissors':
            return 'D'
          case 'Paper':
            return 'L'
        }
        break
    }
  }

  public result(): number {
    return this.fights.reduce<number>((sum, fight) => {
      return sum + this.typeValue(fight.me) + this.statusValue(this.fightResult(fight))
    }, 0)
  }
}

export default (): number => {
  const fights = new RockPaperScissors(fs.readFileSync(path.join(process.cwd(), '/data/day2.txt'), { encoding: 'utf8' }),
  )
  return fights.result()
}

