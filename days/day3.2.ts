import fs from 'node:fs'
import path from 'node:path'

export default (): number => {
  const scoreLetter = (letter: string) => {
    if (/^[A-Z]$/.test(letter)) {
      return letter.charCodeAt(0) - 38
    }

    return letter.charCodeAt(0) - 96
  }

  const elfObjects: Record<number, Set<string>> = {
    0: new Set<string>(),
    1: new Set<string>(),
    2: new Set<string>(),
  }

  return fs.readFileSync(path.join(process.cwd(), '/data/day3.txt'), { encoding: 'utf8' })
                      .split('\n')
                      .filter(line => line.length > 0)
                      .reduce((sum, rucksack, currentIndex) => {
                        elfObjects[currentIndex % 3] = new Set<string>(rucksack.split(''))

                        if (currentIndex % 3 === 2) {
                          return sum + scoreLetter(
                            [...elfObjects[0]].filter(x => elfObjects[1].has(x) && elfObjects[2].has(x))[0]
                          )
                        }

                        return sum
                      }, 0)
}
