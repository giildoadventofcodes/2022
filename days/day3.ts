import fs from 'node:fs'
import path from 'node:path'

export default (): number => {
  const scoreLetter = (letter: string) => {
    if (/^[A-Z]$/.test(letter)) {
      return letter.charCodeAt(0) - 38
    }

    return letter.charCodeAt(0) - 96
  }

  return fs.readFileSync(path.join(process.cwd(), '/data/day3.txt'), { encoding: 'utf8' })
                      .split('\n')
                      .filter(line => line.length > 0)
                      .reduce((sum, rucksack) => {
                        const length = rucksack.length
                        const [part1, part2] = [
                          rucksack.substring(0, length / 2),
                          rucksack.substring(length / 2, length),
                        ]

                        const sameLetters = new Set<string>()
                        for (let i = 0; i < part1.length; i++) {
                          if (part2.includes(part1[i])) {
                            sameLetters.add(part1[i])
                          }
                        }

                        return sum + Array.from(sameLetters).reduce((sum, letter) => sum + scoreLetter(letter), 0)
                      }, 0)
}
