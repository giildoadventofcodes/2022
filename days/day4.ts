import fs from 'node:fs'
import path from 'node:path'

export default (): number => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day4.txt'), { encoding: 'utf8' })
           .split('\n')
           .filter((line) => line.length > 0)
           .reduce<number>((sum, pair) => {
             const [elf1, elf2] = pair.split(',')
             const [elf1Start, elf1End] = elf1.split('-').map((n) => parseInt(n))
             const [elf2Start, elf2End] = elf2.split('-').map((n) => parseInt(n))

             if (elf1Start <= elf2Start && elf1End >= elf2End || elf2Start <= elf1Start && elf2End >= elf1End) {
               return ++sum
             }

             return sum
           }, 0)
}
