import { enter } from '../data/day5'

class Reader {
  private stacks: string[][] = []

  private isMove: boolean = false

  constructor(read: string[]) {
    read.forEach((row, rowIndex) => {
      if (row === '') {
        this.isMove = true
        return
      }

      if (!this.isMove) {
        row.split('').forEach((char, charI) => {
          if ((charI + 3) % 4 === 0 && !Number.isInteger(parseInt(char))) {
            if (rowIndex === 0) {
              this.stacks.push([])
            }

            if (char !== ' ') {
              this.stacks[(charI - 1) / 4]?.push(char)
            }
          }
        })
      } else {
        const {
          moveNb,
          from,
          to,
        } = /^move (?<moveNb>\d+) from (?<from>\d+) to (?<to>\d+)$/.exec(row)?.groups as { moveNb: string, from: string, to: string }
        this.moveStacks(parseInt(moveNb), parseInt(from), parseInt(to))
      }
    })
  }

  private moveStacks(moveNb: number, from: number, to: number) {
    this.stacks[to - 1] = [
      ...this.stacks[from - 1].splice(0, moveNb),
      ...this.stacks[to - 1],
    ]
  }

  public getFirstItemOfStacks(): string {
    return this.stacks.reduce((acc, stack) => acc + stack[0], '')
  }
}

export default (): string => {
  return (new Reader(enter.split('\n'))).getFirstItemOfStacks()
}
