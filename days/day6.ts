import { enter } from '../data/day6'

export default (): number => {
  const lastChars: string[] = []
  const chars = enter[0].split('')
  let testSet = new Set<string>()

  let sequenceIndex = 0
  for (let i = 0; i < chars.length; i++) {
    lastChars.push(chars[i])

    if (i >= 4) {
      lastChars.shift()
      testSet = new Set(lastChars)

      if (testSet.size === 4) {
        sequenceIndex = i +1
        break
      }
    }
  }

  return sequenceIndex
}
