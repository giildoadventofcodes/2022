import { day7 } from '../data/day7'

class FileReader {
  private directories: Map<string, number> = new Map()
  private directoryOpened: string = ''

  constructor() {
    day7.split('\n')
        .forEach(line => {
          if (line === '$ ls') return

          let cdCondition = /^\$ cd (?<dir>\S+)$/.exec(line)?.groups?.dir
          if (cdCondition) {
            this.getDirectory(cdCondition)
            return
          }

          let fileCondition = /^(?<size>\d+) \S+$/.exec(line)
          if (fileCondition) {
            if (this.directoryOpened === '/') {
              this.directories.set('/', (this.directories.get('/') || 0) + Number(fileCondition.groups?.size))
              return
            }

            let directory = this.directoryOpened
            while (directory !== '') {
              this.directories.set(directory, (this.directories.get(directory) || 0) + Number(fileCondition.groups?.size))
              directory = directory.substring(0, directory.lastIndexOf('/'))
            }

            this.directories.set('/', (this.directories.get('/') || 0) + Number(fileCondition.groups?.size))
          }
        })
  }

  private getDirectory(cdCondition: string): void {
    if (cdCondition === '/') {
      this.directoryOpened = '/'

      if (!this.directories.has('/')) {
        this.directories.set('/', 0)
      }

      return
    }

    if (cdCondition === '..') {
      this.directoryOpened = this.directoryOpened.substring(0, this.directoryOpened.lastIndexOf('/'))
      return
    }

    this.directoryOpened = this.directoryOpened === '/'
      ? `/${cdCondition}`
      : `${this.directoryOpened}/${cdCondition}`
  }

  public getLargestDirectory(): number {
    let sumSize = 0

    this.directories.forEach((size) => {
      if (size < 100000) {
        sumSize += size
      }
    })

    return sumSize
  }
}

export default (): number => {
  return (new FileReader()).getLargestDirectory()
}
