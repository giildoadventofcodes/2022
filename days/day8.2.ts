import { day8 } from '../data/day8'

class MapReader {
  private map: number[][]
  private treeVisibles: number
  private treesVisibility: Map<string, { t: number, l: number, r: number, b: number }>

  constructor() {
    this.map = day8.split('\n').map(line => line.split('').map(Number))
    this.treeVisibles = 0

    this.treesVisibility = new Map()

    this.getTreeCount()
  }

  private incrementTreeVisibility(x: number, y: number, direction: 't' | 'l' | 'r' | 'b'): void {
    const visibility = this.treesVisibility.get(`${x},${y}`)
    if (visibility) {
      this.treesVisibility.set(`${x},${y}`, {
        ...visibility,
        [direction]: visibility[direction] + 1,
      })
    }
  }

  private checkTop(x: number, y: number): void {
    for (let i = y - 1; i >= 0; i--) {
      if (this.map[y][x] > this.map[i][x]) {
        this.incrementTreeVisibility(x, y, 't')
      } else {
        this.incrementTreeVisibility(x, y, 't')
        return
      }
    }
  }

  private checkLeft(x: number, y: number): void {
    for (let i = x - 1; i >= 0; i--) {
      if (this.map[y][x] > this.map[y][i]) {
        this.incrementTreeVisibility(x, y, 'l')
      } else {
        this.incrementTreeVisibility(x, y, 'l')
        return
      }
    }
  }

  private checkRight(x: number, y: number): void {
    for (let i = x + 1; i < this.map[y].length; i++) {
      if (this.map[y][x] > this.map[y][i]) {
        this.incrementTreeVisibility(x, y, 'r')
      } else {
        this.incrementTreeVisibility(x, y, 'r')
        return
      }
    }
  }

  private checkBottom(x: number, y: number): void {
    for (let i = y + 1; i < this.map.length; i++) {
      if (this.map[y][x] > this.map[i][x]) {
        this.incrementTreeVisibility(x, y, 'b')
      } else {
        this.incrementTreeVisibility(x, y, 'b')
        return
      }
    }
  }

  private checkAllDirections(x: number, y: number): void {
    this.treesVisibility.set(`${x},${y}`, { t: 0, l: 0, r: 0, b: 0 })

    this.checkTop(x, y)
    this.checkLeft(x, y)
    this.checkRight(x, y)
    this.checkBottom(x, y)
  }

  private getTreeCount(): void {
    for (let y = 0; y < this.map.length; y++) {
      for (let x = 0; x < this.map[y].length; x++) {
        this.checkAllDirections(x, y)
      }
    }
  }

  public getTreeVisibility(): number {
    return [...this.treesVisibility.values()]
      .map(({ t, l, r, b }) => {
        return t * l * r * b
      })
      .sort((a, b) => b - a)[0]
  }
}

export default (): number => {
  return (new MapReader()).getTreeVisibility()
}
