import { day8 } from '../data/day8'

class MapReader {
  private map: number[][]
  private treeVisibles: number

  constructor() {
    this.map = day8.split('\n').map(line => line.split('').map(Number))
    this.treeVisibles = 0

    this.getTreeCount()
  }

  private checkTop(x: number, y: number): boolean {
    return this.map.slice(0, y).every(row => row[x] < this.map[y][x])
  }

  private checkLeft(x: number, y: number): boolean {
    return this.map[y].slice(0, x).every(tree => tree < this.map[y][x])
  }

  private checkRight(x: number, y: number): boolean {
    return this.map[y].slice(x + 1).every(tree => tree < this.map[y][x])
  }

  private checkBottom(x: number, y: number): boolean {
    return this.map.slice(y + 1).every(row => row[x] < this.map[y][x])
  }

  private checkAllDirections(x: number, y: number): boolean {
    return this.checkTop(x, y) ||
      this.checkLeft(x, y) ||
      this.checkRight(x, y) ||
      this.checkBottom(x, y)
  }

  private getTreeCount(): void {
    for (let y = 0; y < this.map.length; y++) {
      for (let x = 0; x < this.map[y].length; x++) {
        if (y === 0 || y === this.map.length - 1 || x === 0 || x === this.map[y].length - 1) {
          this.treeVisibles++
          continue
        }

        if (this.checkAllDirections(x, y)) {
          this.treeVisibles++
        }
      }
    }
  }

  public getTreeVisible(): number {
    return this.treeVisibles
  }
}

export default (): number => {
  return (new MapReader()).getTreeVisible()
}
