import { day9 } from '../data/day9'

type Direction = 'U' | 'D' | 'L' | 'R'

class Plank {
  private tail9CoordinatesList: Set<string>
  private headCoordinates: { x: number, y: number }
  private tailsCoordinates: Record<number, { x: number, y: number }>
  private moves: { direction: Direction, distance: number }[] = []

  constructor() {
    this.headCoordinates = { x: 0, y: 0 }
    this.tailsCoordinates = {
      1: { x: 0, y: 0 },
      2: { x: 0, y: 0 },
      3: { x: 0, y: 0 },
      4: { x: 0, y: 0 },
      5: { x: 0, y: 0 },
      6: { x: 0, y: 0 },
      7: { x: 0, y: 0 },
      8: { x: 0, y: 0 },
      9: { x: 0, y: 0 },
    }

    this.tail9CoordinatesList = new Set([JSON.stringify(this.tailsCoordinates[9])])

    this.moves = day9.split('\n').map((move: string) => {
      const [direction, distance] = move.split(' ')
      return { direction: direction as Direction, distance: parseInt(distance) }
    })

    this.movesHead()
  }

  private saveTailCoordinates() {
    this.tail9CoordinatesList.add(JSON.stringify(this.tailsCoordinates[9]))
  }

  private moveTail(i: number) {
    let distX: number = 0
    let distY: number = 0
    if (i === 1) {
      distX = this.headCoordinates.x - this.tailsCoordinates[1].x
      distY = this.headCoordinates.y - this.tailsCoordinates[1].y
    } else {
      distX = this.tailsCoordinates[i - 1].x - this.tailsCoordinates[i].x
      distY = this.tailsCoordinates[i - 1].y - this.tailsCoordinates[i].y
    }
    let absDistX = Math.abs(distX)
    let absDistY = Math.abs(distY)


    if (absDistX <= 1 && absDistY <= 1) {
      return
    }

    if (absDistX > 1 && absDistY === 0) {
      if (distX > 0) {
        // Suit à droite
        this.tailsCoordinates[i].x++
        this.saveTailCoordinates()
        return
      }

      // Suit à gauche
      this.tailsCoordinates[i].x--
      this.saveTailCoordinates()
      return
    }

    if (absDistX === 0 && absDistY > 1) {
      if (distY > 0) {
        // Suit en haut
        this.tailsCoordinates[i].y++
        this.saveTailCoordinates()
        return
      }

      // Suit en bas
      this.tailsCoordinates[i].y--
      this.saveTailCoordinates()
      return
    }

    if (absDistX === 1 && absDistY > 1 || absDistX === 2 && absDistY === 2) {
      if (distX > 0) {
        this.tailsCoordinates[i].x++

        if (distY > 0) {
          // Suit en haut à droite
          this.tailsCoordinates[i].y++
          this.saveTailCoordinates()
          return
        }

        // Suit en bas à droite
        this.tailsCoordinates[i].y--
        this.saveTailCoordinates()
        return
      }

      this.tailsCoordinates[i].x--
      if (distY > 0) {
        // Suit en haut à gauche
        this.tailsCoordinates[i].y++
        this.saveTailCoordinates()
        return
      }

      // Suit en bas à gauche
      this.tailsCoordinates[i].y--
      this.saveTailCoordinates()
      return
    }

    if (absDistX > 1 && absDistY === 1) {
      if (distY > 0) {
        this.tailsCoordinates[i].y++

        if (distX > 0) {
          // Suit à droite en haut
          this.tailsCoordinates[i].x++
          this.saveTailCoordinates()
          return
        }

        // Suit à gauche en haut
        this.tailsCoordinates[i].x--
        this.saveTailCoordinates()
        return
      }

      this.tailsCoordinates[i].y--
      if (distX > 0) {
        // Suit à droite en bas
        this.tailsCoordinates[i].x++
        this.saveTailCoordinates()
        return
      }

      // Suit à gauche en bas
      this.tailsCoordinates[i].x--
      this.saveTailCoordinates()
      return
    }
  }

  private moveTails(moveNb: string) {
    for (let i = 1; i <= 9; i++) {
      this.moveTail(i)
    }
  }

  private movesHead() {
    this.moves.forEach(({ distance, direction }, j) => {
      switch (direction) {
        case 'U':
          for (let i = 0; i < distance; i++) {
            this.headCoordinates.y++
            this.moveTails(`${j} - ${i}`)
          }
          break
        case 'D':
          for (let i = 0; i < distance; i++) {
            this.headCoordinates.y--
            this.moveTails(`${j} - ${i}`)
          }
          break
        case 'L':
          for (let i = 0; i < distance; i++) {
            this.headCoordinates.x--
            this.moveTails(`${j} - ${i}`)
          }
          break
        case 'R':
          for (let i = 0; i < distance; i++) {
            this.headCoordinates.x++
            this.moveTails(`${j} - ${i}`)
          }
          break
      }
    })
  }

  public getTailCoordinatesList(): number {
    return this.tail9CoordinatesList.size
  }
}

export default (): number => {
  return new Plank().getTailCoordinatesList()
}
