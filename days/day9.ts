import { day9 } from '../data/day9'

type Direction = 'U' | 'D' | 'L' | 'R'

class Plank {
  private tailCoordinatesList: Set<string>
  private tailCoordinates: { x: number, y: number }
  private headCoordinates: { x: number, y: number }
  private moves: { direction: Direction, distance: number }[] = []

  constructor() {
    this.headCoordinates = { x: 0, y: 0 }
    this.tailCoordinates = { x: 0, y: 0 }

    this.tailCoordinatesList = new Set([JSON.stringify(this.tailCoordinates)])

    this.moves = day9.split('\n').map((move: string) => {
      const [direction, distance] = move.split(' ')
      return { direction: direction as Direction, distance: parseInt(distance) }
    })

    this.movesHead()
  }

  private saveTailCoordinates() {
    this.tailCoordinatesList.add(JSON.stringify(this.tailCoordinates))
  }

  private moveTail() {
    const distX = this.headCoordinates.x - this.tailCoordinates.x
    const distY = this.headCoordinates.y - this.tailCoordinates.y
    const absDistX = Math.abs(distX)
    const absDistY = Math.abs(distY)

    if (absDistX <= 1 && absDistY <= 1) {
      return
    }

    if (absDistX > 1 && absDistY === 0) {
      if (distX > 0) {
        // Suit à droite
        this.tailCoordinates.x++
        this.saveTailCoordinates()
        return
      }

      // Suit à gauche
      this.tailCoordinates.x--
      this.saveTailCoordinates()
      return
    }

    if (absDistX === 0 && absDistY > 1) {
      if (distY > 0) {
        // Suit en haut
        this.tailCoordinates.y++
        this.saveTailCoordinates()
        return
      }

      // Suit en bas
      this.tailCoordinates.y--
      this.saveTailCoordinates()
      return
    }

    if (absDistX === 1 && absDistY > 1) {
      if (distX > 0) {
        this.tailCoordinates.x++

        if (distY > 0) {
          // Suit en haut à droite
          this.tailCoordinates.y++
          this.saveTailCoordinates()
          return
        }

        // Suit en bas à droite
        this.tailCoordinates.y--
        this.saveTailCoordinates()
        return
      }

      this.tailCoordinates.x--
      if (distY > 0) {
        // Suit en haut à gauche
        this.tailCoordinates.y++
        this.saveTailCoordinates()
        return
      }

      // Suit en bas à gauche
      this.tailCoordinates.y--
      this.saveTailCoordinates()
      return
    }

    if (absDistX > 1 && absDistY === 1) {
      if (distY > 0) {
        this.tailCoordinates.y++

        if (distX > 0) {
          // Suit à droite en haut
          this.tailCoordinates.x++
          this.saveTailCoordinates()
          return
        }

        // Suit à gauche en haut
        this.tailCoordinates.x--
        this.saveTailCoordinates()
        return
      }

      this.tailCoordinates.y--
      if (distX > 0) {
        // Suit à droite en bas
        this.tailCoordinates.x++
        this.saveTailCoordinates()
        return
      }

      // Suit à gauche en bas
      this.tailCoordinates.x--
      this.saveTailCoordinates()
      return
    }
  }

  private movesHead() {
    this.moves.forEach(({ distance, direction }) => {
      switch (direction) {
        case 'U':
          for (let i = 0; i < distance; i++) {
            this.headCoordinates.y++
            this.moveTail()
          }
          break
        case 'D':
          for (let i = 0; i < distance; i++) {
            this.headCoordinates.y--
            this.moveTail()
          }
          break
        case 'L':
          for (let i = 0; i < distance; i++) {
            this.headCoordinates.x--
            this.moveTail()
          }
          break
        case 'R':
          for (let i = 0; i < distance; i++) {
            this.headCoordinates.x++
            this.moveTail()
          }
          break
      }
    })
  }

  public getTailCoordinatesList():number {
    return this.tailCoordinatesList.size
  }
}

export default (): number => {
  return new Plank().getTailCoordinatesList()
}
